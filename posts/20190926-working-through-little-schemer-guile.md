title: Working through The Little Schemer with Guile and Emacs
date: 2019-09-26 20:21
author: Neil
tags: little-schemer guile emacs
---

I've been reading through the first chapter of The Little Schemer, and following along in Emacs (specifically: [spacemacs](http://spacemacs.org)). I jotted down a few notes on getting set up to do this.

First off you'll need to install a Scheme implementation.  There's a few of them out there.  This [Reddit thread](https://www.reddit.com/r/lisp/comments/b4gr2x/which_scheme_interpreter_should_i_use/
) has some useful discussion on the pros and cons of each of them.

As I'm on Linux and using Emacs, I went with [Guile](https://www.reddit.com/r/lisp/comments/b4gr2x/which_scheme_interpreter_should_i_use/
).

To install on Linux Mint, you just need:

```
sudo apt install guile-2.2
```

At this point, you could simply fire up Guile and work within the REPL there, if you wanted.

I want to write my Scheme in Emacs, and then send it to the REPL from there.  The preferred Emacs package for that seems to be [geiser](https://gitlab.com/jaor/geiser).  In spacemacs, geiser comes included with the [scheme layer](http://spacemacs.org/layers/+lang/scheme/README.html), so all you need to do is add that layer into your config and you've got geiser (and some other handy scheme bits and pieces).

Once you're in a scheme file, run `M-x run-geiser`, choose guile from the dropdown, and that'll start up the Guile REPL and allow you to send parts of your file to it for evaluation. `C-x C-e` for example will send the sexp before the cursor.

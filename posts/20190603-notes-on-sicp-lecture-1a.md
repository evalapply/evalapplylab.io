title: Notes on SICP Lecture 1A
date: 2019-06-03 21:42
author: Neil
tags: sicp
summary: Notes on SICP Lecture 1A
---

Watched the [Lecture 1a video for SICP](https://archive.org/details/halmit1a).
(Not sure how exactly these correspond to the book – is it by chapter?)

It’s Hal Abelson giving the lectures. I find him really engaging. And, I did not
know this until I looked him up just now, but is a founding director of both
Creative Commons and the Free Software Foundation. Rad!

Anyway, here’s a few notes I made while watching the video.

## Not computers, not a science

Off the bat, he calls out computer science as not really being a science. Yup. I
liked here also how he said it’s also not really about computers. To call
computer science is conflating it too much with the tools we associate with it.
But it’s not really about computers.

It’s more about the description of processes, how to do things, and techniques
for dealing with the complexity of those processes as they get larger and
larger.

He touched on the difference between declarative knowledge – a description of
what something is; vs imperative knowledge – describing how something is done.

## Techniques for complexity

Three of the techniques for dealing with complexity that will feature heavily in
the course:

- Black Box Abstractions
- Conventional Interfaces
- Meta-linguistic abstraction (AKA making new languages)

Then he started up on black-box abstractions a bit.

## Black Box Abstractions

- Primitive elements
  - things like +, 3, 17.4, 5
  - you can combine them together to do things with them
- Means of combination
  - `(+ 3 (* 5 6) 8 2)`
  - this is a combination with an operator and operands
  - combinations are really trees
- Means of abstraction
  - being able to define a procedure for doing something, and reusing it
    - e.g. `(define (square x) (* 5 5))`
    - which is syntatic sugar for `(define square (lambda (x) (* x x)))`
  - even better: being able to define general concepts

Once defined, you can use the things you’ve defined as if there were primitives. This is apparently a key thing of Lisp and he sound quite excited by it.

Quick mention of case analysis, like:

```
(define (abs x)
  (cond ((< x 0) (- x))
  (cond ((= x 0) 0)
  (cond ((> x 0) x)))))
```

He then defined Heron of Alexandria’s method for finding square roots by hand:

```
(define (try guess x)
  (if (good-enough? guess x)
     guess
     (try (improve guess x) x)))
```
     
```
(define (sqrt x) (try 1 x))
```

With the point being that it’s built-up of other building blocks, like `improve`
and `good-enough?`. And also that try is recursive (it calls itself, only stopping
when a particular condition is met.)

If you were to package all the procedures for sqrt into one place, it would be
called a block structure.

## Summary

Alright. So I guess in summary: we’re going to be thinking about how we can
represent imperative knowledge. And ways in which we can try to keep on top of
things when we represent BIG chunks of imperative knowledge.

title: Adding blog posts to EvalApply
date: 2019-08-07 20:05
author: Neil
tags: meta
summary: Setting up the blog section of the site.
---

So far, we have been making use of only the pages functionality of Haunt.
However, as a static site generator, it also comes with functionality for
blog posts.  We've just turned this on, so we can also add in blog posts
about what we're covering in EvalApply.

We have already written some blog posts about our progress through SICP, so we
can backfill these posts in on the site.

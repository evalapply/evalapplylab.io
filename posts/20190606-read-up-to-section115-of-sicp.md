title: Read up to section 1.1.5 of SICP
date: 2019-06-06 20:05
author: Neil
tags: sicp
summary: Read up to section 1.1.5 of SICP.
---

Read up to section 1.1.5 of SICP.

Some notes to self of things I found interesting:

## The environment

I like the idea of giving more thought to the environment itself. I’ve always
thought more about the code, less about what the environment is doing.

> the interpreter must maintain some sort of memory that keeps track of the
> name-object pairs. This memory is called the environment.

> the general notion of the environment as providing a context in which
> evaluation takes place will play an important role in our understanding of
> program execution.

## Evaluation

Tree accumulation – a new concept to me. I like trees as structures.

> In general, we shall see that recursion is a very powerful technique for
> dealing with hierarchical, treelike objects. In fact the “percolate values
> upward” form of the evaluation rule is an example of a general kind of process
> known as tree accumulation

## Special forms

Read about special forms before, but it kind of clicked here to read that they
are exceptions to general evaluation and have their own rules.

> Such exceptions to the general evaluation rule are called special forms.
> define is the only example of a special form that we have seen so far, but we
> will meet others shortly. Each special form has its own evaluation rule.

## Applicative-order evaluation vs normal-order evaluation

You can evaluate procedures in different ways.

I think there’s a subtle distinction beween evaluation and procedure
application that I’ve missed and need to look back over.

----

That’ll do for now!

title: EvalApply 30th July 2019
date: 2019-07-30 21:18
author: Neil
tags: evalapply sicp
---

We had a fun session today, during which we decided to add to our
homepage a sentence about our interest in examining programming and technology
in a wider societal context. Early on we discussed that this was important to us
all, and we often end up chatting about these topics when we meet – perhaps more
so than SICP, so far!

We were pondering EvalApply as a name for the group for a short period – from an
early email:

> In addition, thinking about it further, it also has a double meaning to me
> that I really like. 
>
> “Before we apply a function we must first evaluate its
> arguments.” 
> 
> Taken metaphorically I feel that this captures [our]
> philosophical and political views towards technology in a broader sense. We
> consider the social ramifications of technology before recommending its use.
> We evaluate the arguments before applying its function.

Some things that I remember we chatted about today:

The important of constraints, or having a limited palette. I can’t remember how
it came up, but for me it recalled some of the ideas from old tracker music
software and the demoscene, where using restricted hardware and software can be
a useful creative constraint.

We talked about community moderation (further to a short note about it earlier
this week), with Panda making a strong point that not everyone has the resources
to extensively moderate a community. It had come up for me recently in the
context of the Fediverse, and the discussion over the defederation of the Gab
instance, and the problems with freedom 0. “The freedom to run the program as
you wish, for any purpose” – this is not good if the purpose is, let’s say,
neo-nazism.

Dan discussed the philosopher Simondon (an inspiration for Deleuze and Guattari,
I understand), and the topic of alienation and technology. Not just alienation
as a result of losing autonomy in a capitalist system; but also alienation from
technology – not knowing how things work or being able to tinker with them.
Emacs being a beautiful example of software you can see the innards of and
tinker with, should you wish to.

Dan did a bit of SICP.

Dan and Panda chatted about another French philosopher, whose name I have
unfortunately forgotten, and the philosophy of autism.

I read a couple of paragraphs of SICP.

Dan described the difference between technics and technology, which is really
interesting – a distinction between the machines themselves, and the analysis of
them.

![](/images/evalapply-20190730.jpg "Eval'ing and apply'ing in the May Day Rooms")
 

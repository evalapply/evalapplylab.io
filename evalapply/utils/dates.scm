(define-module (evalapply utils dates)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-19)
  #:export (date))

(define (date year month day)
  "Create a SRFI-19 date for the given YEAR, MONTH, DAY"
  (let ((tzoffset (tm:gmtoff (localtime (time-second (current-time))))))
    (make-date 0 0 0 0 day month year tzoffset)))

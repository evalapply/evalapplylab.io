;;; Copyright © 2018 David Thompson <davet@gnu.org>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (evalapply utils sxml)
             (evalapply utils dates)
             (haunt asset)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt html)
             (haunt page)
             (haunt post)
             (haunt reader)
             (haunt reader commonmark)
             (haunt site)
             (haunt utils)
             (commonmark)
             ;; (syntax-highlight)
             ;; (syntax-highlight scheme)
             (srfi srfi-1)
             (srfi srfi-19)
             (sxml match)
             (sxml transform)
             (web uri))

(define %collections
  `(("Recent Posts" "posts.html" ,posts/reverse-chronological)))

(define parse-lang
  (let ((rx (make-regexp "-*-[ ]+([a-z]*)[ ]+-*-")))
    (lambda (port)
      (let ((line (read-line port)))
        (match:substring (regexp-exec rx line) 1)))))

(define (maybe-highlight-code lang source)
  (let ((lexer (match lang
                 ('scheme lex-scheme)
                 ('xml    lex-xml)
                 ('c      lex-c)
                 (_ #f))))
    (if lexer
        (highlights->sxml (highlight lexer source))
        source)))

(define (sxml-identity . args) args)

(define (highlight-code . tree)
  (sxml-match tree
    ((code (@ (class ,class) . ,attrs) ,source)
     (let ((lang (string->symbol
                  (string-drop class (string-length "language-")))))
       `(code (@ ,@attrs)
             ,(maybe-highlight-code lang source))))
    (,other other)))

(define (highlight-scheme code)
  `(pre (code ,(highlights->sxml (highlight lex-scheme code)))))

;; Markdown doesn't support video, so let's hack around that!  Find
;; <img> tags with a ".webm" source and substitute a <video> tag.
(define (media-hackery . tree)
  (sxml-match tree
    ((img (@ (src ,src) . ,attrs) . ,body)
     (if (string-suffix? ".webm" src)
         `(video (@ (src ,src) (controls "true"),@attrs) ,@body)
         tree))))

(define %commonmark-rules
  `((code . ,highlight-code)
    (img . ,media-hackery)
    (*text* . ,(lambda (tag str) str))
    (*default* . ,sxml-identity)))

(define (post-process-commonmark sxml)
  (pre-post-order sxml %commonmark-rules))

(define commonmark-reader*
  (make-reader (make-file-extension-matcher "md")
               (lambda (file)
                 (call-with-input-file file
                   (lambda (port)
                     (values (read-metadata-headers port)
                             (post-process-commonmark
                              (commonmark->sxml port))))))))

(define evalapply-theme
  (theme #:name "evalapply"
         #:layout
         (lambda (site title body)
           `((doctype "html")
             (head
              (meta (@ (charset "utf-8")))

              ; favicons start
              (link (@ (rel "apple-touch-icon") (sizes "180x180") (href "/apple-touch-icon.png")))
              (link (@ (rel "icon") (type "image/png") (sizes "32x32") (href "/favicon-32x32.png")))
              (link (@ (rel "icon") (type "image/png") (sizes "16x16") (href "/favicon-16x16.png")))
              (link (@ (rel "manifest") (href "/site.webmanifest")))
              (meta (@ (name "msapplication-TileColor") (content "#da532c")))
              (meta (@ (name "theme-color") (content "#ffffff")))
              ; favicons end

              (title ,(string-append title " — " (site-title site)))
              ,(stylesheet "reset")
              ,(stylesheet "fonts")
              ,(stylesheet "evalapply"))
             (body
              (div (@ (class "container"))
                   (div (@ (class "nav"))
                        (ul (li ,(link "EvalApply" "/"))
                            (li (@ (class "fade-text")) " ")
                            (li ,(link "SICP" "/sicp.html"))
                            (li ,(link "Resources" "/resources.html"))
                            (li ,(link "Blog" "/posts.html"))
                            (li ,(link "Join" "/come-along.html"))
                            (li ,(link "Code of conduct" "/conduct.html"))
                            (li ,(link "Contact" "/contact.html"))
                            ))
                   ,body
                   (footer (@ (class "text-center"))
                           (p (@ (class "copyright"))
                              "© 2021 EvalApply"
                              ,%cc-by-sa-button)
                           (p "The text and images on this site are
free culture works available under the " ,%cc-by-sa-link " license.")
                           (p "This website "
                              " ("
                              (a (@ (href "https://gitlab.com/evalapply/evalapply.gitlab.io"))
                                 "source")
                              " and "
                              (a (@ (href "/website.html"))
                                 "more info")
                              ") is built with "
                              (a (@ (href "http://haunt.dthompson.us"))
                                 "Haunt")
                              ", a static site generator written in "
                              (a (@ (href "https://gnu.org/software/guile"))
                                 "Guile Scheme")
                              "."))))))
         #:post-template
         (lambda (post)
           `((h1 (@ (class "title")),(post-ref post 'title))
             (div (@ (class "date"))
                  ,(date->string (post-date post)
                                 "~B ~d, ~Y"))
             (div (@ (class "author"))
                  "Author: " ,(post-ref post 'author))
             (div (@ (class "post"))
                  ,(post-sxml post))))
         #:collection-template
         (lambda (site title posts prefix)
           (define (post-uri post)
             (string-append "/" (or prefix "")
                            (site-post-slug site post) ".html"))

           `((h1 ,title)
             ,(map (lambda (post)
                     (let ((uri (string-append "/"
                                               (site-post-slug site post)
                                               ".html")))
                       `(div (@ (class "summary"))
                             (h2 (a (@ (href ,uri))
                                    ,(post-ref post 'title)))
                             (div (@ (class "date"))
                                  ,(date->string (post-date post)
                                                 "~B ~d, ~Y"))
                             (div (@ (class "post"))
                                  ,(first-paragraph post))
                             (a (@ (href ,uri)) "read more ➔"))))
                   posts)))))

(define (static-page title file-name body)
  (lambda (site posts)
    (make-page file-name
               (with-layout evalapply-theme site title body)
               sxml->html)))

(define intro-page
  (static-page
   "Hello"
   "index.html"
   `((h2 "Hello and welcome to EvalApply!")
     (p "We are a collective of people learning to program in Lisp.")
     (p ,(centered-image "/images/evalapply.jpeg" "Eval Apply"))
     (p "We meet regularly to learn, teach, and chat with one another.  We are self-educating in the fundamental and philosophical principles of computer programming through the study of the classic textbook " ,(link "Structure and Interpretation of Computer Programs" "/sicp.html") ".")
     (p "At EvalApply, we are also interested in examining programming and technology in a wider societal context - and this is something that SICP has fostered, with its focus on ideas, not just implementations.")
     )))

(define sicp-page
  (static-page
   "Structure and Interpretation of Computer Programs"
   "sicp.html"
   `((h2 "Structure and Interpretation of Computer Programs")
     (p "SICP, first published in 1985, is a classic of computer programming education.")
     (p "The article " ,(link "Why Structure and Interpretation of Computer Programs matters" "https://people.eecs.berkeley.edu/~bh/sicp.html") " provides a great overview of why SICP is still considered one of the best textbooks of computer science, even now, 30 years after it was first published.")
     (blockquote "SICP is about standing back from the details to learn big-picture ways to think about the programming process.")
     (p ,(centered-image "/images/sicp.jpeg" "SICP front cover")
     (p "SICP uses Scheme as the programming language with which to explain its concepts.")
     (blockquote "Another revolution was the choice of Scheme as the programming language. [...] It was very brave of Abelson and Sussman to teach their introductory course in the best possible language for teaching, paying no attention to complaints that all the jobs were in some other language. Once you learned the big ideas, they thought, and this is my experience also, learning another programming language isn't a big deal; it's a chore for a weekend.")
     (p "At EvalApply, we are also interested in examining programming and technology in a wider societal context - and this is something that SICP has fostered, with its focus on ideas, not just implementations.")
     (blockquote "The idea that computer science should be about ideas, not entirely about programming practice, has since widened to include non-technical ideas about the context and social implications of computing.")))))



(define code-of-conduct-page
  (static-page
   "Code of conduct"
   "conduct.html"
   `((h2 "Code of conduct")
     (p "Welcome to EvalApply, a space for anyone interested in learning to program.")
     (p "Anyone is welcome to join, whether you’re experienced in programming or just starting out and wanting to learn. When getting involved in our open space, we ask everyone to respect our community values by following the code of conduct below.")

     (h3 "We are social")
     (h3 "We are radically open and actively inclusive")

     (p "We wish to be a diverse bunch from all walks of life and with different life experiences. We welcome all people willing to be radically open: of all appearances, genders, sexualities, nationalities, abilities, backgrounds and political leanings.")
     (p "We take time to get to know each other and consider how others may think and feel, and how comments or actions may be perceived in a diverse community. We all make mistakes or assumptions about others, no matter how open we think we are, but please consider the potential impact of your actions on the people around you. When people express discomfort or raise an issue, listen carefully first before reacting.")
     (p "Even if you disagree with something, take the lead from professional critics: they foster healthy conversation and debate without ever attacking people personally. Think about that when responding to others, and ask yourself the question: would I say it to their face?")

     (h3 "We are friendly")
     (p "It's really important that we make people who come to our space feel welcome straight away so that we don’t loss them before they’ve gotten started. You can put people at ease in really simple ways: acknowledging their presence (saying hi), letting them know how it works (what do they need to do next, what can they expect), offering them a ‘way-in’ (a cup of tea, a place to wait, a way to contribute).")

     (h3 "We are hands-on")
     (p "We help each other, whether it is to install a Scheme environment or go through a SICP exercise.")

     (h3 "We do-it-together")
     (p "We discuss our understanding of SICP, where we're at in the course and progress our understanding by working together.")

     (h3 "We have fun")
     (p "Lisp is a beautiful language, and its Scheme dialect is minimalist. Going through SCIP, as a group, is a great way to learn computing concepts in a fun way.")

     (h3 "We are system-changing")
     (p "Lisp was invented in 1958 and is one of the oldest computer language. The Scheme dialect was created at the MIT in the 1970s. As a functional language Lisp and Scheme are also very modern and powerful. By understanding fundamental computing concepts, on which AI was build, and exploring an elegant and powerful language we are looking to empower ourselves and reverse the dumbing down trend of software teaching."))))

(define resources-page
  (static-page
   "Resources"
   "resources.html"
   `((h2 "Resources")
     (h3 "Book")
      (ul 
        (li ,(link "MIT SICP course" "https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-001-structure-and-interpretation-of-computer-programs-spring-2005/"))
        (li ,(link "Interactive version" "https://xuanji.appspot.com/isicp/index.html") " of the SICP book")
        (li ,(link "The Little Schemer" "https://www.ccs.neu.edu/home/matthias/BTLS/")))
     (h3 "Videos")
      (ul
        (li ,(link "Video lectures" "https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-001-structure-and-interpretation-of-computer-programs-spring-2005/video-lectures/") "  by Hal Abelson and Gerald Jay Sussman")
        (li  "The videos are also available from the " ,(link "web archive" "https://archive.org/details/SICP_4_ipod") " and as a " ,(link "YouTube playlist" "https://www.youtube.com/playlist?list=PLE18841CABEA24090"))
        )
     (h3 "Answers")
      (ul
        (li ,(link "Solutions" "http://community.schemewiki.org/?sicp-solutions") " to the SICP exercises"))
     (h3 "Tools")
      (ul
        (li ,(link "DrRacket editor" "https://download.racket-lang.org"))
        (li  "DrRacket's manual page about the ",(link "SICP Collections" "https://docs.racket-lang.org/sicp-manual/index.html")))
     )))

(define come-along-page
  (static-page
   "Come along"
   "come-along.html"
   `((h2 "Come along")
     (p "EvalApply is currenty on hiatus.")
     (p "We thank the " ,(link  "The Mayday Rooms" "https://www.maydayrooms.org/") " for when we met there.")
     (h3 "Upcoming sessions")
     (p "No upcoming sessions.")
     )))

(define contact-page
  (static-page
   "Contact"
   "contact.html"
   `((h2 "Contact")
     (p "EvalApply was started by " ,(link "Daniel" "https://danielnemenyi.net/") ", " ,(link "Neil" "https://doubleloop.net") ", & " ,(link "Panda" "https://gizmonaut.net") " in May 2019.")
     (p "We can be reached at " ,(link "lambda@evalapply.space" "mailto:lambda@evalapply.space") ".")
     )))

(define website-page
  (static-page
   "Website details"
   "website.html"
   `((h2 "Website")
     (p "This website is made with " ,(link "Haunt" "https://dthompson.us/projects/haunt.html")
        ", a static site generator written in Guile Scheme.")
     (p "The source of this site is available " ,(link "here" "https://gitlab.com/evalapply/evalapply.gitlab.io") ".")
     (p "The site is heavily based on " ,(link "dthompson's website" "https://dthompson.us/")
        ", the source for which is " ,(link "available here" "https://git.dthompson.us/blog.git") ".")
     (p ,(link "jakob.space" "https://jakob.space/")
        " (" ,(link "source" "https://git.sr.ht/~jakob/blog") ") "
        " is also very helpful for figuring out how to code a Haunt site.")
     (p "We use Gitlab Pages to host our site - the statically generated pages are deployed to Pages on commit. "
        "You can see the simple config for that " ,(link "here" "https://gitlab.com/evalapply/evalapply.gitlab.io/blob/master/.gitlab-ci.yml") ".")
     )))

(site #:title "evalapply"
      #:domain "evalapply.space"
      #:default-metadata
      '((author . "Eval Apply")
        (email  . "lambda@evalapply.space"))
      #:readers (list commonmark-reader*)
      #:builders (list intro-page
                       sicp-page
                       code-of-conduct-page
                       resources-page
                       come-along-page
                       contact-page
                       website-page
                       (blog #:theme evalapply-theme #:collections %collections)
                       (atom-feed)
                       (atom-feeds-by-tag)
                       ;(static-directory "js")
                       (static-directory "css")
                       (static-directory "fonts")
                       (static-directory "images")
                       (static-directory "favicons" "")))

# evalapply

This is the source for the [evalapply](https://evalapply.space) website.

It is made with [haunt](https://dthompson.us/projects/haunt.html), and based on [dthompson's blog](https://git.dthompson.us/blog.git/).

## building

- Install guix
- Clone this repo
- cd into the folder
- `guix environment`
- `haunt build`
- `haunt serve`

`haunt build` will output the static site files to the `site` directory.

`haunt serve` will serve these files.
The site should be accessible at http://localhost:8080 .

## deployment

All that is required for deployment is to push to the gitlab git remote (e.g.
`git push origin master`). The CI pipeline will copy the files in the `site`
directory to gitlab's static hosting location.
